package com.book.DataSource

object DataType extends Enumeration
{
  type user_book_data_type= Value
  val user_book_train, user_book_test = Value
}

case class bookinfo(book_id: Int, book_title: String, book_picture: String,
                    book_author: String,  book_translator: Option[String] = None,
                    book_publication: String, book_year_press: String, book_price: String,
                    book_score: Double, book_comment_count: String,
                    book_intro: Option[String] = None, book_tag: String, book_main_tag: String)

case class userinfo(user_id: Int, user_number: String, user_home_link: String, user_picture: String)

case class user_book_map(comment_id: Int, user_id: Int, book_id: Int, book_home: String,
                         book_pic: String, book_name: String, user_book_score: Double,
                         book_comment_words: String, book_comment_date: String)
























