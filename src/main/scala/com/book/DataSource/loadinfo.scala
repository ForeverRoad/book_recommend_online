package com.book.DataSource

import com.book.DataSource.DataType.user_book_data_type
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import scala.util.Try

object loadinfo
{
  private val user_info_path = f"/user/jiangyang_i/rec/user_info/"
  private val book_info_path = f"/user/jiangyang_i/rec/books/"
  private val user_book_map_train_path = f"/user/jiangyang_i/rec/train/"
  private val user_book_map_test_path = f"/user/jiangyang_i/rec/test/"

  def tryToGetStr(str: String): Option[String] = {
    str match {
      case "" => None
      case _ => Some(str)
    }
  }
  def getUserInfo(sc: SparkContext): RDD[userinfo] =
  {
    sc.textFile(user_info_path).map(line =>
    {
      val col = line.split("\\|")
      Try(userinfo(col(0).toInt, col(1), col(2), col(3)))
    }).filter(t => t.isSuccess).map(t => t.get)
  }

  def getBookInfo(sc: SparkContext): RDD[bookinfo] =
  {
    sc.textFile(book_info_path).map(line =>
    {
      val col = line.split("\\|")
      Try(bookinfo(col(0).toInt, col(1),col(2), col(3), tryToGetStr(col(4)), col(5),col(6), col(7),
        col(8).toDouble, col(9), tryToGetStr(col(10)), col(11), col(12)))
    }).filter(g => g.isSuccess).map(t => t.get)
  }
  def getUserBookMap(sc: SparkContext, dataType: user_book_data_type): RDD[user_book_map] =
  {
    val path = dataType match
    {
      case DataType.user_book_test => user_book_map_test_path
      case DataType.user_book_train => user_book_map_train_path
    }
    sc.textFile(path).map(line =>
    {
      val col = line.split("\\|")
      Try(user_book_map(col(0).toInt, col(1).toInt,col(2).toInt, col(3), col(4), col(5),col(6).toDouble, col(7), col(8)))
    }).filter(g => g.isSuccess).map(t => t.get)
  }
}
