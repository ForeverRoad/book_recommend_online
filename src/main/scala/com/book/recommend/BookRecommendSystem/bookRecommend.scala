package com.book.recommend.BookRecommendSystem

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}
import recommendALS.useALS

object bookRecommend
{
  def main(args: Array[String]): Unit =
  {
    //屏蔽日志
    Logger.getLogger("org.apache.spark").setLevel(Level.WARN)
    Logger.getLogger("org.apache.eclipse.jetty.server").setLevel(Level.OFF)

    val conf = new SparkConf().setAppName("bookRecommend").setMaster("xiaojukeji")
    val sc = new SparkContext(conf)
    try
    {
      useALS(sc)
    }catch {
      case ex: Exception => println("can not use ASL model")
    }
    finally
    {
      println("done!")
    }
  }
}
