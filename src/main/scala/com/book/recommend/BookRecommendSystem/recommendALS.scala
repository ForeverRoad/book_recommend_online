package com.book.recommend.BookRecommendSystem

import java.text.SimpleDateFormat
import com.book.DataSource.{DataType, bookinfo, user_book_map, userinfo}
import org.apache.spark.SparkContext
import org.apache.spark.mllib.recommendation.{ALS, MatrixFactorizationModel, Rating}
import org.apache.spark.rdd.RDD
import com.book.DataSource.loadinfo.{getBookInfo, getUserBookMap}

object recommendALS
{
  //user I need
  def get_cal_user_book_map_info(user_book_map: RDD[user_book_map]): RDD[(Long, Rating)] =
  {
    user_book_map.filter(g => g.book_comment_date.length == 19).map(g =>
    {
      val date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
      val dt = date.parse(g.book_comment_date)
      ((dt.getTime()/1000) % 10, Rating(g.user_id, g.book_id, g.user_book_score))
    })
  }
  def get_cal_book_info(bookinfo: RDD[bookinfo]): RDD[(Int, String)] =
  {
    bookinfo.map(g =>
    {
      (g.book_id, g.book_title)
    })
  }
  def computerRmse(model: MatrixFactorizationModel, data: RDD[Rating], n: Long): Double =
  {
    val prediction = model.predict((data.map(o => (o.user, o.product))))
    val predictionsAndRatings = prediction.map(g => ((g.user, g.product), g.rating)).join(data.map(g => ((g.user, g.product), g.rating)))
    math.sqrt(predictionsAndRatings.map(e => (e._2._1 - e._2._2)*(e._2._1 - e._2._2)).reduce(_+_)/n)
  }
  def useALS(sc: SparkContext) =
  {
    //load test data
    val comment_info_test_rdd = get_cal_user_book_map_info(getUserBookMap(sc, DataType.user_book_test))
    //load train data
    val comment_info_train_rdd = get_cal_user_book_map_info(getUserBookMap(sc, DataType.user_book_train))
    val commentCount = comment_info_train_rdd.count()
    val userCount = comment_info_train_rdd.map(_._2.user).distinct()count()
    val bookCount = comment_info_train_rdd.map(_._2.product).distinct().count()
    println("comment count is" + commentCount + "\tuser count is " + userCount + "\tbook count is " + bookCount)
    //load all books
    val books = get_cal_book_info(getBookInfo(sc)).collect().toMap
    //set train set
    val partitionCount = 4
    val training: RDD[Rating] = comment_info_train_rdd.filter(o => o._1 < 6).values.union(comment_info_test_rdd.map(g => g._2)).repartition(partitionCount).persist()
    val validation: RDD[Rating] = comment_info_train_rdd.filter(o => o._1 >= 6 && o._1 < 8).values.repartition(partitionCount).persist()
    val test: RDD[Rating] = comment_info_train_rdd.filter(g => g._1 >= 8).values.persist()
    println("Training count is " + training.count() + "\tvalidating count is " + validation.count() + "\ttest count is " + test.count())

    val ranks = List(8, 12)
    val lambdas = List(0.1, 10.0)
    val numlters = List(10, 20)
    var bestModel: Option[MatrixFactorizationModel] = None
    var bestValidationRmse = Double.MaxValue
    var bestRank = 0
    var bestLambdas = -1.0
    var bestNumlter = -1

    for (rank <- ranks; lambda <- lambdas; numlter <- numlters)
    {
      val model = ALS.train(training, rank, numlter, lambda)
      val validationRmse = computerRmse(model, validation, validation.count())
      println("RMSE(validation) = " + validationRmse + "for the model trained with rank =" + rank + ",lambda = " + lambda + ",and numlter = " + numlter)
      if(validationRmse < bestValidationRmse)
      {
        bestModel = Some(model)
        bestValidationRmse = validationRmse
        bestRank = rank
        bestNumlter = numlter
        bestLambdas = lambda
      }
    }
    val testRmse = computerRmse(bestModel.get, test, test.count())
    println("The best model was trained with rank = " + bestRank + ",lambda = " + bestLambdas + ", numlter = "
      + bestNumlter + ",and its RMSE on the test set is " + testRmse)
    val myRatedBooks = comment_info_test_rdd.map(_._2.product).collect().toSet
    val candidate_book = sc.parallelize(books.keys.filter(!myRatedBooks.contains(_)).toSeq)
    val recommendation_books = bestModel.get.predict(candidate_book.map((0, _))).collect().sortBy(-_.rating).take(10)
    var i = 1
    println("books recommended for you:")
    println("all book is :" + recommendation_books.length)

    recommendation_books.foreach(r =>
    {
      println("first:")
      println("%2d".format(i) + ":" + books(r.product))
      i += 1
    })
  }
}
